﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletSpeed;

    void Start()
    {
        GetComponent<Rigidbody2D>().AddForce(transform.right * bulletSpeed);       //Shoots the bullet
    }
}
