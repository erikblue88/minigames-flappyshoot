﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public float speed = 500f, threeBulletsDuration = 7f, fasterShootDuration = 7f;

    void Start()
    {
        Destroy(gameObject, 10f);       //Destroys gameobject after X secs
        transform.position = new Vector3(transform.position.x, Random.Range(-1f * FindObjectOfType<PlayerMovement>().mapWidth, (float)FindObjectOfType<PlayerMovement>().mapWidth), transform.position.z);      //Selects a random position on the Y axis
        GetComponent<Rigidbody2D>().AddForce(Vector3.right * -speed);      //Makes gameobject move towards the player
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))     //If gameobject collides with the Player
        {
            if (tag == "ThreeBullets")      //If this is "ThreeBullets" powerup
                FindObjectOfType<Shoot>().ThreeBullets(threeBulletsDuration);

            else if (tag == "FasterShoot")      //If this is "FasterShoot" powerup
                FindObjectOfType<Shoot>().FasterShoot(fasterShootDuration);

            Destroy(gameObject);        //Destroys gameobject
        }
        else if (collision.CompareTag("Obstacle"))      //If gameobject collides with an Obstacle
            Destroy(gameObject);        //Destroys gameobject
    }
}
