﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] obstacles, powerUps;
    public GameObject token;
    public float timeBetweenSpawns = 1f, reduceTimeBy = 0.05f, minTimeBetweenSpawns = 0.3f;
    public int tokenFrequency = 3, powerUpFrequency = 3, secondObstacleAfter, thirdObstacleAfter;

    private GameObject tempObstacle = null, tempToken = null;
    private int score;
    private bool secondObstacle = false, thirdObstacle = false;

    public void Spawn()
    {
        if (!FindObjectOfType<GameManager>().gameIsOver)        //If the game is not over yet
        {
            if (timeBetweenSpawns - reduceTimeBy >= minTimeBetweenSpawns)       //If the spanwTime can be reduced
                timeBetweenSpawns -= reduceTimeBy;      //Then reduces it

            if (!secondObstacle)
                tempObstacle = Instantiate(obstacles[0], transform.position, Quaternion.identity);        //Spawns the first Obstacle to the position of the Spawner
            else if (secondObstacle && !thirdObstacle)
                tempObstacle = Instantiate(obstacles[1], transform.position, Quaternion.identity);        //Spawns the second Obstacle to the position of the Spawner
            else
                tempObstacle = Instantiate(obstacles[Random.Range(1, obstacles.Length)], transform.position, Quaternion.identity);        //Spawns a random Obstacle to the position of the Spawner

            CheckObstacle();

            if (Random.Range(0, tokenFrequency) == 0)       //If it is time to spawn a Token
                Invoke("SpawnToken", timeBetweenSpawns / 2f);       //Spawns Token
            if (Random.Range(0, powerUpFrequency) == 0)       //If it is time to spawn a PowerUp
                Invoke("SpawnPowerUp", timeBetweenSpawns / 2f);       //Spawns a random PowerUp

            Invoke("Spawn", timeBetweenSpawns);     //Calls the function 'Spawn' again
        }
    }

    public void SpawnToken()
    {
        if (!FindObjectOfType<GameManager>().gameIsOver)        //If the game is not over yet
            Instantiate(token, transform.position, Quaternion.identity);        //Spawns a Token to the position of the Spawner
    }

    public void SpawnPowerUp()
    {
        if (!FindObjectOfType<GameManager>().gameIsOver)        //If the game is not over yet
            Instantiate(powerUps[Random.Range(0, powerUps.Length)], transform.position, Quaternion.identity);        //Spawns a PowerUp to the position of the Spawner
    }

    public void CheckObstacle()
    {
        score = FindObjectOfType<ScoreManager>().score;

        if (!secondObstacle)
        {
            if (score >= secondObstacleAfter)
            {
                secondObstacle = true;
                Camera.main.GetComponent<Animation>().Play("ZoomOutAnim");
                FindObjectOfType<PlayerMovement>().mapWidth = 8f;
            }
        }

        if (!thirdObstacle)
        {
            if (score >= thirdObstacleAfter)
            {
                thirdObstacle = true;
            }
        }
    }
}
